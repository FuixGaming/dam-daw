import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * 
 * @author David Ramos, Alejandro Vicente y Pablo Jim�nez
 * <p> This is a simple Java program created to investigate and experiment with Code Reviewing
 * and Testing.</p>
 * <p> It's a special calculator to predict how much damage you'd inflict on a hypothetical
 * opposing Pokemon or what the chances of catching one are</p>
 * 
 * There is only one main class, with 3 subfunctions, we'll go into details for each of them.
 */

public class Main {

	/**
	 * 
	 * @param args
	 * 
	 * The main function has an input Scanner as well as a basic menu for selecting which
	 * subfunctions the user would like to execute.
	 */
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("--x Welcome to the Pokemon Calculator x--");
		System.out.println("What would you like to calculate?");
		menu(1);
		int select=0;
		select = input.nextInt();
		
		switch (select) {
		case 1: damage(); break;
		case 2: catchrate(); break;
		case 3: System.out.println("Exiting..."); break;
		default: System.out.println("Invalid input"); break;
		}
		
	}
	
	/**
	 * 
	 * @param x
	 * 
	 * The menu function receives a number for a specific menu to print it and show it to the user.
	 */
	public static void menu(int x) {
		
		switch(x) {
		case 1:
			System.out.println("1- Damage");
			System.out.println("2- Catch Rate");
			System.out.println("3- Exit");
			break;
		case 2:
			System.out.println("1- Yes");
			System.out.println("2- No");
			break;
		case 3:
			System.out.println("1- x2");
			System.out.println("2- x4");
			System.out.println("3- neutral");
			System.out.println("4- x1/2");
			System.out.println("5- x1/4");
			System.out.println("6- Non effective");
			break;
		case 4:
			System.out.println("1- 1 Target");
			System.out.println("2- More than 1 targets");
			break;
		case 5:
			System.out.println("1- Benefitial");
			System.out.println("2- Neutral");
			System.out.println("3- Bad");
			break;
		case 6:
			System.out.println("1- x1");
			System.out.println("2- x1.5");
			System.out.println("3- x2");
			System.out.println("4- x3");
			System.out.println("5- x3.5");
			System.out.println("6- x4");
			System.out.println("7- x5");
			System.out.println("8- x8");
			System.out.println("9- Master Ball");
			break;
		case 7:
			System.out.println("1- Asleep");
			System.out.println("2- Frozen");
			System.out.println("3- Paralyzed");
			System.out.println("4- Poisoned");
			System.out.println("5- Burned");
			System.out.println("6- No status change");
			break;
		}
		
	}
	
	/**
	 * The damage function is one of the two main features of the program.
	 * 
	 * <p>It's the method that calculates the damage inflicted on the rival Pokemon.
	 * It has many different inputs, used for the calculator to get all the variables it needs in
	 * order to calculate the precise number of HP the attack will remove</p>
	 * 
	 * <p>Damage has a small random value, which will make the damage range between two possible
	 * values, so we have made it so that the program shows both values</p>
	 */
	
	public static void damage() {
		
		int sel=0;
		Scanner input = new Scanner(System.in);
		Scanner namein = new Scanner(System.in);
		Scanner doublein = new Scanner(System.in);
		System.out.println("Insert the necessary data");
		System.out.print("Name: ");
		String name = namein.nextLine();
		System.out.print("Level: ");
		int level=0;
		level = input.nextInt();
		while(level<1 || level>100) {
			System.out.println("Invalid level");
			System.out.print("Insert again: ");
			level = input.nextInt();
		}
		System.out.print("Attack/Special Attack: ");
		int attack=0;
		attack = input.nextInt();
		while(attack<1 || attack>999) {
			System.out.println("Invalid attack");
			System.out.print("Insert again: ");
			attack = input.nextInt();
		}
		System.out.print("Defense/Special Defense of the enemy: ");
		int defense=0;
		defense = input.nextInt();
		while(defense<1 || defense>999) {
			System.out.println("Invalid defense");
			System.out.print("Insert again: ");
			defense = input.nextInt();
		}
		System.out.println("Do you have STAB?");
		menu(2);
		double stab=0;
		while(sel!=1 && sel!=2) {
			sel = input.nextInt();
			switch(sel) {
			case 1: stab=1.5; break;
			case 2: stab=1; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.print("Attack Base Power: ");
		int base=0;
		base = input.nextInt();
		while(base<1) {
			System.out.println("Invalid base power");
			System.out.print("Insert again: ");
			base = input.nextInt();
		}
		System.out.println("Effectiveness:");
		menu(3);
		double effectiveness=0;
		while(sel<1 || sel>6) {
			sel = input.nextInt();
			switch(sel) {
			case 1: effectiveness=2; break;
			case 2: effectiveness=4; break;
			case 3: effectiveness=1; break;
			case 4: effectiveness=0.5; break;
			case 5: effectiveness=0.25; break;
			case 6: effectiveness=0; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.println("Targets: ");
		menu(4);
		double target=0;
		while(sel!=1 && sel!=2) {
			sel = input.nextInt();
			switch(sel) {
			case 1: target=1; break;
			case 2: target=0.75; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.println("Benefitial weather:");
		menu(5);
		double weather=0;
		while(sel<1 || sel>3) {
			sel = input.nextInt();
			switch(sel) {
			case 1: weather=1.5; break;
			case 2: weather=1; break;
			case 3: weather=0.5; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.println("Physical attack and burned?");
		menu(2);
		double burned=0;
		while(sel!=1 && sel!=2) {
			sel = input.nextInt();
			switch(sel) {
			case 1: burned=0.5; break;
			case 2: burned=1; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.println("Critical hit?");
		menu(2);
		double critical=0;
		while(sel!=1 && sel!=2) {
			sel = input.nextInt();
			switch(sel) {
			case 1: critical=1.5; break;
			case 2: critical=1; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		System.out.print("Other modifiers: ");
		double other = doublein.nextDouble();
		
		double random=0.85;
		double modifier = target*weather*critical*random*stab*effectiveness*burned*other;
		double mindamage = (((((2*level/5)+2)*base*attack/defense)/50)+2)*modifier;
		mindamage = Math.round(mindamage);
		
		random=1;
		modifier = target*weather*critical*random*stab*effectiveness*burned*other;
		double maxdamage = (((((2*level/5)+2)*base*attack/defense)/50)+2)*modifier;
		maxdamage = Math.round(maxdamage);
		
		System.out.println("The total damage can range from "+mindamage+" to "+maxdamage);
		
	}
	
	/**
	 * catchrate is the second main function and it calculates the chances of catching a wild
	 * Pokemon
	 * 
	 * <p>The catchrate is affected by many variables, so receives input for all of them</p>
	 */
	
	public static void catchrate() {
		
		int sel=0;
		Scanner input = new Scanner(System.in);
		Scanner doublein = new Scanner(System.in);
		
		System.out.println("Insert the necessary data");
		System.out.print("Max HP: ");
		int maxhp=0;
		maxhp = input.nextInt();
		while(maxhp<1 || maxhp>999) {
			System.out.println("Invalid input");
			System.out.print("Insert again:");
			maxhp = input.nextInt();
		}
		System.out.print("Insert the current HP: ");
		int currenthp=0;
		currenthp = input.nextInt();
		while(currenthp<1 || currenthp>maxhp) {
			System.out.println("Invalid input");
			System.out.print("Insert again:");
			currenthp = input.nextInt();
		}
		System.out.print("Insert the catch rate: ");
		int rate=0;
		rate = input.nextInt();
		while(rate<3 || rate>255) {
			System.out.println("Invalid input");
			System.out.print("Insert again:");
			rate = input.nextInt();
		}
		System.out.println("Insert the pokeball bonus: ");
		menu(6);
		double bonusball=0;
		while(sel<1 || sel>9) {
			sel = input.nextInt();
			switch(sel) {
			case 1: bonusball=1; break;
			case 2: bonusball=1.5; break;
			case 3: bonusball=2; break;
			case 4: bonusball=3; break;
			case 5: bonusball=3.5; break;
			case 6: bonusball=4; break;
			case 7: bonusball=5; break;
			case 8: bonusball=8; break;
			case 9: bonusball=9; break;
			default: System.out.println("Invalid input");
			}
		}
		boolean master=false;
		if(bonusball==9) {master=true;}
		sel=0;
		System.out.println("Insert the status of the Pokemon");
		menu(7);
		double status=0;
		while(sel<1 || sel>6) {
			sel = input.nextInt();
			switch(sel) {
			case 1: case 2: status=2; break;
			case 3: case 4: case 5: status=1.5; break;
			case 6: status=1; break;
			default: System.out.println("Invalid input");
			}
		}
		sel=0;
		
		double crateper=0;
		if(master) {
			crateper=100;
		} else {
			double crate = ((3*maxhp-2*currenthp)*rate*bonusball*status)/(3*maxhp);
			crateper = crate*100/255;
		}
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		System.out.println("The catch rate is "+df.format(crateper)+"%");
		
	}
	
}

/**
 * 
 * Links used for reference
 * https://bulbapedia.bulbagarden.net/wiki/Damage
 * https://bulbapedia.bulbagarden.net/wiki/Catch_rate
 * 
 */ 
